<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BasicController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET","HEAD"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $institutions = $this->getDoctrine()
            ->getRepository('AppBundle:Picture')
            ->findAll();

        return $this->render('AppBundle:Basic:index.html.twig', array(
            'Institution' => $institutions
        ));
    }

}
