<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Picture
 *
 * @ORM\Table(name="picture")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PictureRepository")
 * @Vich\Uploadable
 */
class Picture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=127)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="text", nullable=true, unique=false)
     */
    private $picture;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="picture")
     */
    private $user;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Vich\UploadableField(mapping="picture_file", fileNameProperty="picture")
     * @var File
     *
     * @Assert\NotBlank(message="Пожалуйста, укажите изображение")
     * @Assert\File(
     *     mimeTypes = { "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}."
     * )
     * @Assert\Image(
     *     minWidth = 100,
     *     minWidthMessage = "Слижком маленькая ширина изображения ({{ width }}px).
     * Минимальная ширина: {{ min_width }}px.",
     *     maxWidth = 1800,
     *     maxWidthMessage = "Слишком широкое изображение ({{ width }}px).
     * Максимальная ширина: {{ max_width }}px.",
     *     minHeight = 100,
     *     minHeightMessage = "Слишком маленькая высота изображения ({{ height }}px).
     * Минимальная высота: {{ min_height }}px.",
     *     maxHeight = 1800,
     *     maxHeightMessage = "Слишком высокое изображение ({{ height }}px).
     * Максимальная высота: {{ max_height }}px."
     * )
     */

    private $imageFile;

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Picture
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return Picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set caption
     *
     * @param string $caption
     *
     * @return Picture
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishDate", type="datetime")
     */
    private $publishDate;


    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     *
     * @return Picture
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    public function __toString()
    {
        return $this->caption ?: '';
    }
}